from config import *
from time import time


def test_run(ip):
    print(f"{RED}Test is performed for {convert_ip_to_country(ip)} ")
    driver = [None] * len(list_of_stage_urls)
    for iterable, url in enumerate(list_of_stage_urls):
        options = Options()
        options.add_extension(mod_header)
        options.headless = False
        driver[iterable] = webdriver.Chrome(options=options, executable_path=path_to_driver)

        driver[iterable].get(f"https://webdriver.bewisse.com/add?X-Forwarded-For={ip}")
        driver[iterable].execute_script(f"window.open('{url}', 'new_window')")
        sleep(3)
        driver[iterable].switch_to.window(driver[iterable].window_handles[1])
        sleep(5)
        data = driver[iterable].execute_script("return window.dataLayer")

        index = 0
        while True:
            try:
                assert type(data[index]) == dict and len(data[index]) == 3 and data[index]["event"] == "geo"
            except AssertionError:
                index += 1
                if len(data) == index:
                    print(f"{WHITE}Custom event in DataLayer for {url} is {RED}missing{WHITE} in "
                          f"{convert_ip_to_country(ip)} location.")
                    break
                if len(data[index]) == 3 and type(data[index]) == dict and data[index]["event"] == "geo":
                    assert data[index]["eventCategory"] == 'converted'
                    break

        driver[iterable].quit()


if __name__ == "__main__":

    """Positive run"""
    test_run(ip_address["Australia"])
    test_run(ip_address["Austria"])
    test_run(ip_address["Canada"])

    test_run(ip_address["Denmark"])
    test_run(ip_address["Germany"])
    test_run(ip_address["Ireland"])

    test_run(ip_address["New Zealand"])
    test_run(ip_address["South Africa"])
    test_run(ip_address["Sweden"])

    test_run(ip_address["Switzerland"])
    test_run(ip_address["United Kingdom"])
    test_run(ip_address["United States"])

    """"Negative run"""
    print("Negative run")
    test_run(ip_address["Portugal"])
